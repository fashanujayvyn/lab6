/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package airpot;

/**
 *
 * @author jayvy
 */
public class Airline {
    private String name;
    private String shortCode;
    
    public Airline(String name, String shortCode){
        this.name=name;
        this.shortCode=shortCode;
        
    }
    
    public String getName(){
     return name;   
        
    }
    
    public String getshortCode(){
     return shortCode;   
        
    }
    public void setName(String name){
     this.name=name;   
        
    }
    public void setShortcode(String shortCode){
        
     this.shortCode=shortCode;   
    }
    
}
