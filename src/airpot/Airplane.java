/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package airpot;

/**
 *
 * @author jayvy
 */
public class Airplane {
    
    private int id;
    private int numberOfseats;
    private String make;
    private String model;
    
    
    
    public Airplane(int id, int numberOfseats, String make, String model){
        this.id=id;
        this.make=make;
        this.model=model;
        this.numberOfseats=numberOfseats;
        
        
        
    }
    public int getId(){
        return id;
    }
    
    public int getNumberOfSeats(){
        
     return numberOfseats;   
    }
    
    public String getMake(){
    return make; 
    
    }
    
public String getModel(){    
    
 return model;   
    
}
    
    public void setId(int id){
    this.id=id;    
        
    }
    
   public void setNumberOfSeats(int numberOfseats){
       
       this.numberOfseats=numberOfseats;
   }
   
   public void setMake(String make){
       this.make=make;
       
   }
   
   public void setModel(String model){
       this.model=model;
       
       
       
   }
    
    
    
    
}
