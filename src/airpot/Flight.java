/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package airpot;

import java.util.Date;

/**
 *
 * @author jayvy
 */
public class Flight {
    
    private String code;
    private Date date;
    
    public Flight(String code, Date date){
        
        this.code=code;
        this.date=date;
    }
    public String getCode(){
     return code;   
    }
    public Date getDate(){
     return  date;   
    }
    public void setCode(String code){
     this.code=code;   
        
    }
    public void setDate(Date date){
        
     this.date=date;   
    }
    
}
