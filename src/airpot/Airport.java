/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package airpot;

/**
 *
 * @author jayvy
 */
public class Airport {
    
    private String code;
    private String city;
    
    
    public Airport(String code, String city){
        this.city=city;
        this.code=code;
        
        
        
    }
    
    public String getCode(){
        
     return code;   
    }
    public String getCity(){
        
     return city;   
    }
    public void setCode(String code){
        
        this.code=code;
        
    }
    public void setCity(String city){
        
        
     this.city=city;   
    }
    
    
}
